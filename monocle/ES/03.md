# Vistas

Las vistas en Monocle son las que nos permiten representar por pantalla los datos de un modelo en concreto. Por ello cada vista de Monocle tiene un modelo asociado, al cual puedes acceder directamente desde la vista.

## Definición de una vista

Definición de una vista en Monocle:

	class TaskItem extends Monocle.View
	
## Asignar un contenedor a una vista

Es necesario asignarle contendora las vistas, el container es el lugar donde se renderizará nuestra plantilla.

    	container: "ul#tasks"

## Asignar la plantilla a una vista

Podemos asignar una plantilla por valor o por referencia, en el primer caso definimos entre triples comillas dobles la plantilla en html directamente sobre el código de nuestra clase. 

	class TaskItem extends Monocle.View
    	template:
        	"""
        	<li>
            	<strong>{{name}}</strong>
            	<small>{{description}}</small>
        	</li>
        	"""
Por referencia simplemente apuntaríamos al fichero donde guardamos nuestra plantilla.

	template_url: "templates/taksItem.html"
	
## Capturar un evento de una vista

Las vistas nos permiten capturar todo lo que pasa en ellas, nos permite capturar los eventos que suceden sobre ellas y definirles un comportamiento.

    	events:
        	"click li": "onClick"

    	onClick: (event) ->
        	console.error "Current Item", @model
        	
## Definir elementos en una vista

Otra funcionalidad de las vistas es controlar los elementos que hay en ella. Podemos asignar el elemento strong de nuestra plantilla a @name y invocar los métodos que nos provee Quo directamente sin acceder al selector.
   		
    	elements:
        	"strong": "name"

    	exampleMethod: -> @name.html "new content"
    	
## Renderizar una vista

Para renderizar una vista tenemos varios métodos en función de tus necesidades.

Pasandole por parámetro el modelo al crear la vista.

	view = new TaskItem model: data
	
Añadir la vista al contenedor

	view.append task
	
Añadir en primer lugar al contenedor

	view.prepend task
	
Mediante html

	view.html task

## Eliminar una vista

Eliminar la vista y el modelo asociado.

	view.remove()
	
## Actualizar una vista
Refrescar una vista sirve para actualizar los datos que se visualizan en ella en caso de que hayan cambiado.

	view.refresh()
