# The first App with LungoJS

This chapter will explain you how to develop a simple application with LungoJS. It will consist of a task organizer, where the user will be able to see his or her tasks, handle them and create new ones. 

As it has been explained, the acces library to the DOM [QuoJS](https://github.com/soyjavi/quojs) will be necessary and, of course, [LungoJS](https://github.com/tapquo/Lungo.js).

For this example, we will use the language CoffeeScript. In order to compile the code in JavaScript it can be used an automation tool like [Grunt](http://gruntjs.com/getting-started) in order to make this process transparent. 

As MVD framework to help the development, it will be used [Monocle](https://github.com/soyjavi/monocle). 


We propose to use this structure:


1.  An app file, that contains a file with the CSS of the application, a file containing the compilations of the code that it generates, that is, the JavaScript files, another containing the HTML files that are not included in the index.hatml and a index.html file with the main HTML.

2.	 A src file, that contains a file with the components to be used, in this case LungoJS, QuoJS and Monocle, another file containing the styles to be defined and another file with the code to be used. As Monocle has been chosen, this divides the code into controllers, views and models, thus, we recommend you to have different files; controllers, models and views. Moreover, we recommend you to have a file where you include the common code, for example a Proxy, inside the one that contains the code.

In this case the structure would be:

![image](../images/22-structure.png)

In the index.html must be imported the files that contain the compilations of the app code, thus, the JavaScript files, the CSS and the components. In our case, we link the components and we carry out the function "uglify" with the GRUNT in order to have them in the same file. 

Here you have how we import all these in the index.html

	<!doctype html>
    <html>
    <head>
        <meta charset="utf-8">
        <title>Example</title>
        <meta name="description" content="">
        <meta name="author" content="Tapquo (@tapquo)">
        <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <link rel="stylesheet" href="css/example.components.css">
        <link rel="stylesheet" href="css/example.css">
    </head>

    <body>
        <section id="dashboard" data-transition="slice">
            <article id="list_tasks"></article>
        </section>
        <script src="js/example.components.js"></script>
        <script src="js/example.js"></script>
    </body>
    </html>
   
In order to start LungoJS, it is necessary to run the next function when you start the application; to do this, we will use the common file for our code, in this case we have called it "app.coffe".

	Lungo.init({"name": "example"});
 
Once you have done this, you are ready to start developing the application.

To start with, we will create the main section, which will allow us to make tasks lists and create new ones:

    <section id="dashboard" data-transition="slice">
        <header data-title="Tareas">
            <nav>
                <button data-icon="plus" data-action="new_task"></button>
            </nav>
        </header>
        <article id="list_tasks" class="active list scroll">
            <ul></ul>
        </article>
    </section>
 
Ans would be displayed in the screen like this:

![image](../images/22-tasks.png)

As Monocle is a MVC framework, we recommend to create a controller for each section. Therefore, we will create the controller "dashboard.coffee", inside the file "controllers" that we hace created, which will be in charge of handling the "dashboard" section. As we will create tasks, we must also create the "task.coffee" model, and the view for when those tasks are rendered. 

Thus, we create the fiel "rask.coffe" inside "models", and we define the model. 

	class __Model.Task extends Monocle.Model

      @fields "id", "title", "description", "state", "duration", "planned_at", "difficulty"

      state: -> "check-empty"
 
And its basic view, in the file "task_list_item.coffee" inside the file "views":

	class __View.TaskListItem extends Monocle.View

      container: "#list_tasks ul"

      template_url: "templates/task_list_item.mustache"

      constructor: ->
        super
        @append @model

In this example, we do not include the "template" in the view, we define it in a separate file. In this way, you have to add to the view the path of this file in the "template_url" attribute. In our case, inside the file app, we create another one called "templates" where we save the file with the "template" with the extension "mustache". The file would be this way:

	<li class="{{difficulty}}">
        <span class="icon {{state}}"></span>
        <span class="on-right tag count">{{duration}} h</span>
      <div class="on-right margin_right">{{planned_at}}  </div>
      <strong>{{title}}</strong>
      <small>{{description}}</small>
	</li>

All this, is explained in the manual of Monocle, so we will no explain it now. 

Monocle has the ability of gathering events, but as this manual is made for LungoJS, the development will be made using the own collecting system of this framework. 

The definition of the controller of "dashboard" is made this way:

	class DashboardCtrl extends Monocle.Controller

      constructor: ->
        super
        
	Lungo.ready ->
      __Controller.Dashboard = new DashboardCtrl "body"
   
We will start now developing functionalities:

The first functionality we will develop is to create a new task. To do it, we will collect the event of push in the button +, which is located in the header of the left. 

We add the tap of that event in the constructor of that controller:

    Lungo.dom("[data-action=new_task]").on "tap", (event) =>
      Lungo.Router.section("task")
      
Thus, when clocking the add button, it will be displayed a section with "id" "task", which contains the form to create a new task. Then, we add that section to the file "views" that has been created in the app file, where there will be all the HTML that are not in the index.html. As it has been explained, these HTML must be launched in the function Lungo,init() the following way:

	Lungo.init
      name: "example"
      resources:[
        "views/task.html"
      ]
      
As you can see, we have call the file containing our new section "task.html", and contains:

    <section id="task" data-transition="slide">
        <header data-title="Tarea" data-back="chevron-left"></header>
        <article id="form" class="active indented scroll">
            <div class="form">
                <fieldset>
                    <label>Título</label>
                    <input type="text" name="title" placeholder="Título...">
                </fieldset>
                <fieldset>
                    <textarea placeholder="Descripción..." name="description"></textarea>
                </fieldset>
                <fieldset>
                    <label>Dificultad</label>
                    <label class="select">
                        <select class="custom" name="difficulty">
                            <option value="accept">Fácil</option>
                            <option value="warning">Normal</option>
                            <option value="cancel">Dificil</option>
                        </select>
                    </label>
                </fieldset>
                <fieldset>
                    <label>Planeada</label>
                    <input type="date" name="planned_at">
                </fieldset>
                <label id="duration_label">Duración: 5 horas</label>
                <input id="duration" type="range" placeholder="type your name" min="1" max="9" value="5">
       			<button class="anchor accept" data-action="save_task" data-icon="ok" data-label="Aceptar"></button>
            	<button class="anchor disabled" data-icon="ok" data-label="Aceptar"></button>            
            </div>
        </article>
    </section>


Now, we will create a new controller to control this section, we have called it "task.coffee", and we will include a methos in order to initialize the form that will be run in the moment that is shown:

    class TaskCtrl extends Monocle.Controller

      elements:
        "#duration_label"          : "duration_label"
        "#duration"                : "duration"
        "[name=title]"             : "title"
        "[name=description]"       : "description"
        "[name=difficulty]"        : "difficulty"
        "[name=planned_at]"        : "planned_at"
        "[data-action=save_task]"  : "btn_accept"
        ".disabled"                : "btn_disabled"

      initForm: ->
        @title.val ""
        @description.val ""
        @difficulty.val "accept"
        @planned_at.val ""
        @duration.val "5"  
        @duration_label.html "Duración: 5 horas"
        @btn_accept.hide()
        @btn_disabled.show()

    Lungo.ready ->
        __Controller.Task = new TaskCtrl "body"
        
        
Therefore, when collecting the button event to create a task, it must be added the call of that function to initialize:

    Lungo.dom("[data-action=new_task]").on "tap", (event) =>
      __Controller.Task.initForm()
      Lungo.Router.section("task")

As you can see in the HTML of the "task.html", we have added in the header a `data-title="Tarea"` that states the tittle of the header and a `data-back="chevron-left"` which establishes a button on the left of the header to return to the previous section. In the article there is a normal "input" to add the tittle of the task, a "textarea" to add its description, a selector to define the difficulty of the task, depending on the difficulty the line located on ht left of hte list will be green to indicate easy, yellow toindicate normal and red to indicate difficult, a calendar indicating when is planned to make it and range to select the hours that the task will last to be made. 

That section will be seen in the screen this way:

![image](../images/22-form.png)

## Range
The first thing we are going to do is to collect the event in the constructor of the "task.coffee" controller of when the range is changed, so when this happens, the label that indicates the value of that range is updated.
	
    constructor: ->
      super
      Lungo.dom("#duration").on "change", (event) =>
        @duration_label.html "Duración: #{@duration.val()} horas" 
      
In other words, when the value of the element that has "id" "duration" changes, in this case corresponds to the range, the text with that value of the element, that in this controller is called "duration_label", will change.

## Disabled butoon
Now you will learn how to create a new task. We will force the fields title and description to be obligatory. To do it, as you can see, the LungoJS button displayed has the class "disabled". Its goal is to give the user the feeling that he or she cannot click on it until these two fields are filled. 

In this example, every time that the user clicks on key of the field title or description, it will be checked if one of these fields are empty, to do it we will start collecting thsese two events in the "task" controller:

    Lungo.dom("[name=title]").on "keyup", (event) =>
      @onCheckAccept()

    Lungo.dom("[name=description]").on "keyup", (event) =>
      @onCheckAccept()

Both of them call the function in charge of displaying the correct button. In case that any of the two fields are empty the "disabled" button will be displayed, and if not, the button that runs the action of creating a task will be displayed. 

    onCheckAccept: ->
      if @title.val() isnt "" and @description.val() isnt ""
        @btn_accept.show()
        @btn_disabled.hide()
      else
        @btn_accept.hide()
        @btn_disabled.show()

## Create a task 
Now you have to collect the event of clicking in the accept button in this controller, and that button will create the new task.:

    Lungo.dom("[data-action=save_task").on "tap", (event) =>
      @onSaveTask()
      
Thus, when you click on the accept button, the "onSaveTask()" function is called. This function collects all the fields of the form, creates the task and returns to the section and the article where the tasks list is located. 
 
    onSaveTask: ->
      data =
        title       : @title.val()
        description : @description.val()
        difficulty  : @difficulty.val()
        planned_at  : @planned_at.val()
        duration    : @duration.val()

      task = __Model.Task.create data
      Lungo.Router.back()
   
## List tasks
You already now how to create tasks, but now you will learn how to list them in the main article, which will be done thanks to Monocle. We collect in the controller´s constructor "dashboard.coffee", as they will list in the "dashboard" section the "binding" that will be activated when a new task is created. 

    __Model.Task.bind "create", @onCreate
   
This "binding", when activated, calls the "onCreate()" function, which calls the view in order to list in the conrresponding article. 

    onCreate: (task) ->
      new __View.TaskListItem model: task

## finish tasks
You already know how to create and view tasks, now you will be able to finish them. To do it, when clicking on the task, we will make the task ended and that icon will be changed to know which of them are finished. 

In the "task" view, we will collect the "tap" event with Monocle:

	events:
      "onChangeState": "onChangeState"
     
And we create the onChangeState() function that modifies the task´s state. We have used the state in order to define the icone that is displayed in the tasks list, being "check" finished and "check-empty" unfinished. 

    onChangeState: ->
      if @model.state is "check"
        @model.updateAttributes state: "check-empty"
      else
        @model.updateAttributes state: "check"
      @refresh()
      
The list with finished and unfinished tasks would be:


![image](../images/22-list_tasks.png)

##Progress bar

Now we will add a progress bar with LungoJS that shows the percentage of the tasks made versus all you can find in the list. firstly, we add in the HTML just below the tasks list, thus, below the <ul></ul> tags. 

    <div class="form margin_top">
        <div id="progress" data-progress="0%"></div>
    <div>
    
Therefore, when we modify the state of a task and when we create anew one, the progress bar must be changed. 

Thus, you should add to the onChangeState() function of the "task" view, which is in charge of changing the state of the task, a call to the function in charge of modifing the progress bar. It would be this way:

    onChangeState: ->
      if @model.state is "check"
        @model.updateAttributes state: "check-empty"
      else
        @model.updateAttributes state: "check"
      @refresh()
      __Controller.Dashboard.calculatePercent()
      
At the same time, you also have to add that call to the "onSaveTask" method of the "task" controller, which is in charge of creating the task. It would be this way:

    onSaveTask: ->
      data =
        title       : @title.val()
        description : @description.val()
        difficulty  : @difficulty.val()
        planned_at  : @planned_at.val()
        duration    : @duration.val()

    task = __Model.Task.create data
    __Controller.Dashboard.calculatePercent()
    Lungo.Router.back()


This "calculatePercent" function is defined in the "dashboard.coffee" controller, as the preogress bar and the tasks list are in the "dashboard" section. 

    calculatePercent: ->
      count_total = $$("#list_tasks").find("li").length
      count_finish = $$("#list_tasks").find(".check").length
      percent = if count_total is 0 then 0 else count_finish * 100 / count_total
      Lungo.Element.progress("#progress", percent)
 
The application with the preogress bar would be this way:

![image](../images/22-progress.png)

##Pull & Refresh

Now we will add pull & refresh to the tasks list. To start with, the `data-pull="home"` attribute must be added to the "dashboard" section. In this example, we will load the tasks again, but it could be that instead of listing only our tasks, tasks of other people are also listed, so the pull & refresh should be more useful to list all tasks created by other people. The sectionwith the new attribute would be this way:


    <section id="dashboard" data-transition="slice" data-pull="arrow-down">
        <header data-title="Tareas">
            <nav>
                <button data-icon="plus" data-action="new_task"></button>
            </nav>
        </header>
        <article id="list_tasks" class="active list scroll" >
            <ul></ul>
            <div class="form margin_top">
                <div id="progress" data-progress="0%"></div>
            <div>
        </article>
    </section>

And in the constructor of the "dashboard.coffee" controller we initialize the pull&refresh element using CoffeeScript this way:

    pull = new Lungo.Element.Pull("#list_tasks",
      onPull: "Actualizar las tareas"
      onRelease: "Se van a acualizar las tareas"
      onRefresh: "Actualizando las tareas..."
      callback: =>
        @list_tasks.html("")
        _renderView(task) for task in __Model.Task.all()
        pull.hide()
    )
 
As you can see, the "@list_tasks" element is used, which is defined as element by Monocle:

    elements:
      "#list_tasks ul": "list_tasks"
    
The "_renderView" methos is in charge of calling the view in order to list each task:

    _renderView = (task) ->
      new __View.TaskListItem model: task

We have created this functionality from a submethod as in the "binding" for creating a task a call to the view is also done. Thus, the "onCreate" function would be this way:

    onCreate: (task) -> _renderView(task)

Here you have how the pull&refresh would be displayed in the app:

![image](../images/22-pull_refresh.png)


##Aside

Now we will add an aside menu to our application. To begin with, we will define it in HTML. We will put it in a separate .html file, thus, in the "views" file, inside the "app" file. We will call it "menu.html"

The defined HTML for our menu is the following:

	<aside id="menu">
        <header data-title="Tapquo"></header>
        <article class="active list">
            <ul>
                <li data-icon="list"  data-view-article="list_tasks" data-title="Tareas">
                    <strong>Listar tareas</strong>
                </li>
                <li data-icon="ban-circle">
                    <strong>Eliminar finalizadas</strong>
                </li>
                <li data-icon="remove-circle">
                    <strong>Eliminar todas</strong>
                </li>
                <li data-icon="pencil" data-view-article="profile" data-title="Perfil">
                    <strong>Editar perfil</strong>
                </li>
            </ul>
        </article>
    </aside>

Do not forget to add this file to the Lungo.init() function. It would be:

	Lungo.init
      name: "example"
      resources:[
        "views/task.html",
        "views/menu.html"
      ]

In order to merge this menu to our main section, it is necessary to add the `data-aside="menu"` attribute to that section and in order to display it you must add a button with that function, thus, a button containing the `data-view-aside="menu"` attribute. As the menu will be displayed on the left side, we will put the add task button on the right and the menu button on the left. The section would be this way:

    <section id="dashboard" data-transition="slice" data-pull="arrow-down" data-aside="menu">
        <header data-title="Tareas">
            <nav>
                <button data-icon="menu" data-view-aside="menu" ></button>
            </nav>
            <nav class="on-right">
                <button data-icon="plus" data-action="new_task"></button>
            </nav>
        </header>
        ...
    </section>
    
The aside menu in our app would be displayed this way:

   
![image](../images/22-aside.png)

As you can see, there are four actions. The first one will be to return to the article for listing the tasks, the second one to remove the finished tasks, the third one to remove all tasks, and the last one to edit the user´s profile. As you can see, there are two actions with the `data-title` property, so when you click on them the title of the header will change. The las action also has the `data-view-article="profile"` attribute, so when you click in that list element, the "id" "profile" article will be displayed. Thus, we will create that article in the dashboard section: 


    <article id="profile" class="indented">
        <div class="form">
            <fieldset>
                <label>Nombre:</label>
                <input type="text" name="name" value="Tapquo"/>
            </fieldset>
            <button class="anchor accept" data-action="change_name" data-label="Modificar nombre"></button>
        </div>
    </article>


In the application would be displayed this way:

![image](../images/22-profile.png)

As you can see, the add task button remains, but we only want it when the tasks are being listed, so we will add to that button the `data-article="list_tasks"` attribute:

    <button data-icon="plus" data-action="new_task" data-article="list_tasks"></button>

## Edit name

The only thing you have to do in the "profile" article is change the user´s name, that is written in the aside menu. We will start developing this functionality. Firstly, we collect the event of the button in the constructor of the "dashboard.coffee" controller:

    Lungo.dom("[data-action=change_name]").on "tap", (event) =>
      @_changeName()
     
and we define the function, which will modify the name in the aside menu, and will display it in order to indicate the change. 

    _changeName: ->  
      $$("#menu header h1").html @name.val()
      Lungo.Aside.show("menu")
 
As you can see, "@name" is used, an element defined by Monocle the following way: 

    elements: 
      ...   	 
      "#profile [name=name]" : "name"  
         
## Remove tasks
Now we will develop the other functionalitites, carried out from the aside menu: one for removing the finished tasks, and the other for removing all the tasks. To do it, firstly, we collect the "tap" events about those menu elements, in the "dashboard.coffee" constructor: 
    	
    Lungo.dom("li[data-icon=ban-circle]").on "tap", (event) =>
      @_deleteTask(true)

    Lungo.dom("li[data-icon=remove-circle]").on "tap", (event) =>
      @_deleteTask()

And we state the function that is run once collected these events, which is in charge of removing the corresponding tasks, being all of them in case of clicking on "remove all" or only those finished in case of clicking on "remove finished". Then, the new percentage will be calculated in order to modify the pregress bar, and the aside menu will be hidden and the tasks list displayed: 

    _deleteTask: (finished) ->
      tasks = if finished then __Model.Task.finished() else __Model.Task.all()
      task.destroy() for task in tasks
      __Controller.Dashboard.calculatePercent()
      Lungo.Aside.hide()
      Lungo.Router.article("dashboard", "list_tasks")
      
As you can see, in the function, a method recently added to the "task" model is called, which will return the finished tasks:

	 @finished: () ->
       @select (task) -> task if task.state is "check"
 
and on the view we have added the "binding" that is activated when removing a task model of Monocle

    __Model.Task.bind "destroy", @bindDelete
  
When activated, the next function is called, which removes the view of the removed task, which causes that the user can not see it in the screen.  

     bindDelete: (model) =>
       if model.uid is @model.uid
        @destroy()
                
## Confirmation of notification

Now we will add a LungoJS notification, which will ask if we really want to remove the tasks.

To do it, in the functions that collect the "tap" events in "Remove finished" and "Remove all", instead of calling the function that removes the tasks, now it will be called the function that shows the notification:

    Lungo.dom("li[data-icon=ban-circle]").on "tap", (event) =>
      @_confirmDelete(true)

    Lungo.dom("li[data-icon=remove-circle]").on "tap", (event) =>
      @_confirmDelete()
 
That funciton is this:

    _confirmDelete: (finished) ->
      text = if finished then "las tareas finalizadas" else "todas las tareas"
      Lungo.Notification.confirm
        icon: "remove"
        title: "¿Seguro?"
        description: "Se van a eliminar #{text}. ¿Estás seguro?"
        accept:
          icon: "checkmark"
          label: "Aceptar"
          callback: =>
            @_deleteTask finished

        cancel:
          icon: "close"
          label: "Cancelar"
          callback: -> @
   
As you can see, if the user clicks on the accept button, the function that removes the corresponding tasks is called, and if not, does nothing. 

## Success notifications

Now, we will create a success notification of LungoJS when a task is finished. To do it, in the view, in the "onChangeState" function, if the state changes into "check", thus, the task is finished, we will add the following call:

    Lungo.Notification.success "Felicidades", "Has acabado tu tarea", "check", 2

## Menu
Finally, we will create a navigation menu in the header, to do it, we put the add task button on the left again. And on the right we put the button in charge of displaying the menu. This button must have the `data-view-menu="options"` attribute. The header, thus, would be:

    <header data-title="Tareas">
        <nav>
            <button data-icon="menu" data-view-aside="menu"></button>
            <button data-icon="plus" data-action="new_task" data-article="list_tasks"></button>
        </nav>
        <nav class="on-right">
            <button href="#" data-view-menu="options" data-icon="menu"></a>
        </nav>
    </header>

This attribute displays the navigation menu that has as "id" "options". It would be defined inside the section the following way:

     <nav id="options" data-control="menu" >
        <a href="#" data-view-article="list_tasks"  data-title="Tareas" data-icon="list" data-label="Listar tareas"></a>
        <a href="#" data-view-article="profile" data-icon="pencil" data-label="Editar perfil"  data-title="Perfil"></a>
        <a href="#" data-icon="menu" data-label="Ocultar"></a>
     </nav>

In the application, it will be displayed this way:


![image](../images/22-menu.png)

As you can see in the HTML, the first option is to show the article of the tasks list, the second option is to show the article that edits the user´s name, and the third option hides the menu. 