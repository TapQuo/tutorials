# Grid

LungoJS offers this functionality, which allows to separate the content. Through this, you can divide the space from a container. The size is diveded in proportion to the size of this. This framework calculates the proportion of the space having into account the `data-layout="primary"` and `data-layout="secondary"` attributes that contain the subelements of that container. 

In this way, if a container´s element has the `data-layout="primary"` attribute, this attribute will take up the largest space. If there is also a `data-layout="secondary"`, this will occupy less space than `data-layout="primary"` but more than a subelement that does not have those attributes.

Knowing this, you just have to make combinations until you obtain the wanted distribution.

For this to work, that container must have the class `layout` if you want a vertical division, that is to say, that what is divided is the height of the container. On the contrary, it must have the class `layout horizontal` if you want to divide the width of the container. 

Here you have an example with the existing divisions:

    <section id="main" >
        <header data-title="Grid"></header>
        <article id="grid" class=" active">
            <div class="layout">
                <div data-layout="primary" style="background: #ddd;">&nbsp;</div>
                <div data-layout="secondary" style="background: #999;">&nbsp;</div>
                <div style="background: #ddd;">&nbsp;</div>
            </div>
        </article>
    </section>
   
In the example, it is represented a container, in this case a `<div>` containing the class `layout`, that is to say, the division will be based on the height. Inside this container, it can be seen a buscontainer that does not have any attribute, the second one has the `data-layout="primary"` attribute so it will be the container that takes most space, followed you find the subcontainer that represents the `data-layout="secondary"` attribute and thus, this subcontainer will occupy less than the previous one but more than the first one. 

In the following image, you can see those divisions:


![image](../images/15-basic.png)

As it has been said, these attributes can be combined in order to reach the wanted structure. For example, if you want to divide the height in two big and equal parts, followed by two smaller and, next, another one even smaller, you just have to to the following:

    <section id="main" >
        <header data-title="Grid"></header>
        <article id="grid" class=" active">
            <div class="layout">
                <div data-layout="primary" style="background: #ddd;">&nbsp;</div>
                <div data-layout="primary" style="background: #bbb;">&nbsp;</div>
                <div data-layout="secondary" style="background: #999;">&nbsp;</div>
                <div data-layout="secondary" style="background: #6E6E6E;">&nbsp;</div>
                <div style="background: #585858;">&nbsp;</div>
            </div>
        </article>
    </section>
   
This would be divided the following way:

![image](../images/15-complex.png)

In order to state the horizontal division:

    <section id="main" >
        <header data-title="Grid"></header>
        <article id="grid" class=" active">
            <div class="layout horizontal">
                <div data-layout="primary" style="background: #ddd;">&nbsp;</div>
                <div data-layout="primary" style="background: #bbb;">&nbsp;</div>
                <div data-layout="secondary" style="background: #999;">&nbsp;</div>
                <div data-layout="secondary" style="background: #6E6E6E;">&nbsp;</div>
                <div style="background: #585858;">&nbsp;</div>
            </div>
        </article>
    </section>
  
It would be divided in this way:


![image](../images/15-complex_horizontal.png)
