# Data-attributes

En LungoJS hay una serie de atributos que se ponen dentro de algunos elemetos HTML que empiezan por data-*, que facilitan al desarrollador su trabajo. Algunos de ellos ya se han explicado según surgían en el tutorial, pero no está demás tener claros todos los que este framework nos ofrece y recordar los ya vistos.

## attributes

### icon

El `data-icon` crea un icono vectorizado. Únicamente al escribir `data-icon="user"` en la pantalla ya veremos el icono de un usuario. El valor de este atributo corresponde con el nombre de un icono de LungoJS. 

A continuación se muestra un ejemplo de como queda en HTML un elemento de lista con este atributo:
	
	
	<div class="list">
		<ul>
			<li data-icon="user">
    			<strong>Usuario</strong>
        		<small>Icono de un usuario</small>
    		</li>
		</ul>
	</div>

Y así se muestra por pantalla:

![image](../images/07-data_icon.png)


### image

El `data-image` genera automáticamente el atributo HTML `<img>` y en cuyo atributo "src" se asignará el valor que le hayamos asignado en el `date-image`. Esto es una forma sencilla de agregar en elementos HTML una imagen. Un ejemplo en HTML de esto en una lista sería:

	<div class="list">
		<ul>
    		<li data-image="http://cdn.tapquo.com/lungo/icon-144.png">
        		<strong>Imagen</strong>
        		<small> Imagen con data-imagen </small>
    		</li>
    	</ul>
	</div>

Y se mostraría así:

![image](../images/07-data_image.png)


### progress

Este elemento ya se ha visto en los elementos de los formularios. Simplemente añadiendo `data-progress` a un elemento HTML `<div>` se mostrará una barra de progreso, este debe de ir dentro de otro `<div>` con clase "form". El valor del `data-progress` es un tanto por ciento, que representa del 0 al 100 cuanto debe de ir avanzada la barra. A continuación se muestra un ejemplo:

     <div class="form">
	     <div id="progress" data-progress="25%"></div>
     <div>

Esto queda representado asi:

![image](../images/07-data_progress.png)

### loading

Este elemento muestra automáticamente un icono animado que indica que algo está en proceso. Un ejemplo del `data-loading` en una lista:

    <article id="form-special" class="list">
    	<ul>
        	<li data-loading=""></li>
    	</ul>
    </article>
   
Se muestra así:
   
![image](../images/07-data_loading.png)


### title

Este elemento sirve para dos cosas. La primera para asignarle un título a el encabezado. A continuación se muestra un ejemplo.

	<header data-title="Configuration"></header>
	
La segunda, para todos los elementos HTML `<button>` o `<a>` que lleven este atributo, al pinchar en ellos, el título del encabezado cambia y se muestra el valor que hay en dicho atributo. Por ejemplo:

    <nav>
        <a href="#" data-article="friends" data-title="Amigos"></a>
    </nav>
 
Con este ejemplo, al pinchar en ese botón, en el título del encabezado se mostraría: "Amigos"

### label
En algunos elementos HTML, al añadirle el atributo `data-label` se pondrá automáticamente el texto que se le haya puesto como valor a dicho atributo. Un ejemplo de dicho atributo en un botón:
     
    <button data-icon="signin" data-label="Recordar"></button>

En el botón del ejemplo aparecerá el texto "Recordar".

### count
En algunos elementos HTML se puede añadir este atributo. Lo que hace es poner un número pequeño a modo de contador. Un ejemplo de como sería poner este contador en un botón de un pie de página:

	<footer>
        <nav>
            <a href="#" data-view-article="art1" data-icon="user" data-count="20" class="active"></a>
            <a href="#" data-view-article="art2" data-icon="inbox" data-count="80"><</a>
        </nav>
    </footer>

Y ahora se muestra como queda este pie de página en la pantalla:    


![image](../images/07-data_count.png)


### back
Este atributo se puede poner dentro de un encabezado, y generará un botón que se representará con el icono que lleve por nombre el valor de dicho atributo. Al pinchar en él, te llevará automáticamente a la sección que se haya mostrado anteriormente. Un ejemplo:

	<header data-title="Layout" data-back="chevron-left"> </header>

Como se ve en el ejemplo, en dicho encabezado se generará un botón que tendrá de icono una flecha hacia la izquierda y que al pinchar en ella te llevará a la sección anterior.