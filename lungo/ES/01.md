#TUTORIAL DE LUNGO

A través de este tutorial, se aprende a utilizar el framework LungoJS, el cual permite crear aplicaciones web compatibles con todos los dispositivos. El tutorial explica toda la funcionalidad de este framework a lo largo de varios capítulos.

## Dependecias

Lo primero que se debe hacer para empezar a trabajar con este framework es incluir LungoJS en el proyecto, y su dependencia necesaria que es QuoJS. Aquí se muestra como incluir tanto los ficheros JavaScript como los ficheros CSS de LungoJS y QuoJS en el HTML principal.


    <!doctype html>
    <html>
    <head>
        <meta charset="utf-8">
        <title>Example</title>
        <meta name="description" content="">
        <link rel="stylesheet" href="components/lungo/lungo.css">
        <link rel="stylesheet" href="components/lungo/lungo.icon.css">
        <link rel="stylesheet" href="components/lungo/lungo.theme.css">
    </head>

    <body>
        <section id="main_section">
            <article></article>
        </section>
        <script src="components/quojs/quo.js"></script>
        <script src="components/lungo/lungo.js"></script>
    </body>
    </html>
	
Una vez hecho esto, para iniciar LungoJS hace falta ejecutar la siguiente función enviando como parámetro un objeto que como mínimo contenga el nombre de la aplicación.

	Lungo.init(
      name: "Example"
      version: "1.0"
      history: false
      resources: []
  	)

## Carga de los recursos síncronos al inicializar Lungo

Para que sea más sencillo crear y modificar una aplicación, se pueden crear las secciones y otros elementos HTML en ficheros separados y cargarlos de manerá síncrona al iniciar la aplicación, dejando el fichero HTML principal mucho más limpio y organizando mejor el código.

Para ello, en el objeto que se le envía a la función que se ejecuta para iniciar LungoJS, "Lungo.init(data)", hay que incluir el atributo "resources" que es un array de rutas que corresponderán con las rutas a nuestros ficheros HTML.

	Lungo.init({
    	name: 'example',
    	resources: ['section_to_load.html']
	});


## Carga de los recursos asíncronos mediante enlance
Si en cambio se quieren cargar estos ficheros de una forma asíncrona, hay que añadir a la etiqueta `<a>` el atributo `data-async` con el enlace a tu sección.

	<section id="loader" data-transition="">
    	<article id="art1" class="active">
        	<a href="#" data-view-section="main" data-async="section_to_load.html">
           		Go to section
        	</a>
    	</article>
	</section>
