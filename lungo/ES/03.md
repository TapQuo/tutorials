# Navegación

La navegación de LungoJS se realiza de una manera semántica, dentro de las etiquetas `<a>` y `<button>`. Para ello, en dichos elementos hay que añadir un atributo más.

## Atributo data-view-*

En caso de que se quiera mostrar una sección al pulsar en un botón, el atributo a añadir en dicho botón será `data-view-section="section"` cuyo valor será la "id" de la sección que se quiera mostrar. Un ejemplo:
	
	<button data-view-section="share" data-icon="share"></button>

En caso de que lo que se quiera mostrar sea un artículo determinado, el atributo a añadir será `data-view-article="article"` cuyo valor será la "id" de el artículo que se quiera mostrar. Un ejemplo:

	<button data-view-article="article1" data-icon="user"></button>

Y si lo que se quiere mostrar es un menú lateral, el atributo a añadir será `data-view-aside="aside"` cuyo valor será la "id" de dicho menú. Un ejemplo:

	<button data-view-aside="aside" data-icon="menu"></button>
	
Aquí se muestra un ejemplo sencillo de una sección con dos artículos, cada artículo posee un botón que te lleva a el otro artículo:

    <section id="main">
        <article id="article_1" class="active">
            <button data-view-article="article_2" data-icon="forward" data-label="To article_2"></button>
        </article>
        <article id="article_2">
            <button data-view-article="article_1" data-icon="home" data-label="To article_1"></button>
        </article>
    </section>

## Atributo data-back

Hay una manera rápida de poner en el encabezado un botón que te lleve a la sección anterior. Para ello hay que añadirle a la etiqueta `<header>` el atributo `data-back="home"`, el valor de dicho atributo corresponde con el nombre de un icono que proporciona LungoJS, que será el que representa a el botón.

        <header data-back="home"></header>


## Barra de navegación

Como ya se ha explicado en el capítulo anterior, se puede hacer navegación en los encabezas y en los pie de página incluyendo en estos un elemento de navegación, representado en HTML mediante la etiqueta `<nav>`.

En un encabezado:

    <header data-title="title">
        <nav>
            <button data-view-article="article1" data-icon="home"></button>
        </nav>
        <nav class="on-right">
            <button data-view-section="section1" data-icon="user"></button>
        </nav>
    </header>

En un pie de página:

	<footer>
        <nav>
            <a href="#" data-view-article="article1" data-icon="home"></a>
            <a href="#" data-view-article="article2" data-icon="user"></a>
            <a href="#" data-view-section="section3" data-icon="right"></a>
        </nav>
    </footer>
 

### Groupbar

LungoJS te da la capacidad de tener una barra de navegación estilada justo debajo del encabezado. Para ello, después de la definición del encabezado hay que añadir un elemento de navegación: `<nav>` con el atributo `data-control="groupbar"`. Por tanto, el grupo de botones debe de ir dentro de su etiquetado `<nav data-control="groupbar">` y `</nav>`. Aquí se muestra un ejemplo:

	<nav data-control="groupbar">
        <a href="#" data-view-article="article1" data-label="Profile"></a>
        <a href="#" data-view-article="article2" data-label="Map"></a>
        <a href="#" data-view-article="article3" data-label="Settings"></a>
    </nav>

![image](../images/03-groupbar.png)

## Menú
LungoJS también ofrece un menú desplegable, el cual al pinchar en un botón se muestra una lista de opciones. Esto se hace añadiendo a el elemento HTML `<a>` o `<button>` el atributo `data-view-menu="options"` cuyo valor pertenece a la "id" del elemento de navegación que contiene los botones que se van a desplegar. Por tanto, también hay que definir un elemento de navegación con dicha "id". Este tiene la característica que debe contener el atributo `data-control="menu"`. Un ejemplo del botón: 

    <a href="#" data-view-menu="options" data-icon="menu"></a>

y un ejemplo de el elemento de navegación:

	<nav id="options" data-control="menu">
    	<a href="#" data-view-article="article1" data-icon="menu" data-label="Home"></a>
    	<a href="#" data-view-article="article2" data-icon="globe" data-label="Explore"></a>
    	<a href="#" data-view-article="article3" data-icon="comments" data-label="comments"></a>
    	<a href="#" data-view-article="article4" data-icon="user" data-label="Profile"></a>
	</nav>

Este menú se despliega de la siguiente manera:

![image](../images/03-menu.png)


Si lo que queremos en vez de esto, es una lista con los iconos y un pequeño texto, es decir, queremos darle más importancia a los iconos, sólo habría que añadir a el elemento de navegación la clase "icons". Un ejemplo:

	<nav id="options" data-control="menu" class="icons">
    	<a href="#" data-view-article="article1" data-icon="menu" data-label="Home"></a>
    	<a href="#" data-view-article="article2" data-icon="globe" data-label="Explore"></a>
    	<a href="#" data-view-article="article3" data-icon="comments" data-label="comments"></a>
    	<a href="#" data-view-article="article4" data-icon="user" data-label="Profile"></a>
	</nav>
 
Este menú se desplegaría de la siguiente manera:

![image](../images/03-menu_icons.png)
