# Capturing events 

A very useful thing for an element from your HTML is to be subscribed to an event. When an event  is triggered over an element it is possible to capture it and define a behaviour. This is done by listeners, functions that are subscribed to certain changes inside an element.

In this chapter you will learn how to subscribe to an ecent, capture it and define the behaviour through a callback. 

## Add or remove a listener

In order to add a listener to an element, you have to access it and, using the on function, you add the listener to the event, which name has to be passed as first parameter, as the second parameter you have to state the function you want to run when the event is captured. 

	$$('#push-button').on('click',function() { alert('ALERT');})
	
In order to remove a listener, you just have to call the off function with the selected element and specify the event´s name from you would like to end the subscription. 

	$$('#push-button').off('click')
	
## Trigger an event

You can also cause a particular event over an element; thanks to the function trigger you trigger an event in the selected element. 

	$$('#push-button').trigger('click')


## Event ready

In order to be sure that all your code is emplyed when all the DOM elements are created and your assets are loaded you have the event ready. This function triggers the callback that works as a parameter when the elements are loaded. It is very common to call this function in order to guarantee the correct functioning of your QuoJS´s code. 

	$$(document).ready(function() { alert('I´m ready!');})