# Tutorial de Yoi

Yoi es un servidor basado en la tecnogía NodeJS muy sencillo pero capaz de suplir muchas de las necesidades mas habituales de un servidor web y muchas otras herramientas de uso general. Yoi está escrito en CoffeeScript para una mayor legibilidad y mantenibilidad. 

Con este tutorial aprenderemos como sacarle provecho a nuestro servidor Yoi partiendo desde las necesidades mas básicas a algunas mas avanzadas. 

Primero comprenderemos un poco la estructura de Yoi y como configurarlo para cada entorno, despues veremos como levantar un servidor Yoi, para despues crear paso a paso un servidor web y una API Rest.

 También aprenderemos como testear nuestra propia API. Con todo esto ya aprendido aprenderemos como realizar Crons para planificar la ejecución dentro de nuestro Servidor y como realizar un Crawlers a partir de nuestro Yoi para poder scrapear contenidos.

Una vez con la base ya aprendida veremos como crear y consumir servicios de terceros con Yoi, por último veremos como realizar deploys a nuestro servidor Yoi de manera automatizada.

Como punto de partida para poder seguir este tutorial de manera fluida es requisito el comprender el lenguaje [CoffeScript](https://leanpub.com/coffeescript) y se recomienda conocer como funciona la tecnología [NodeJS](http://nodejs.org/) a modo de servidor y su estructura de paquetes [npm](https://www.npmjs.org/). Asimismo es recomendable conocer algunas de las tecnologías que citaremos a lo largo del tutorial, como bases de datos NoSQL (MongoDB) y la interfaz web REST.

Para poder empezar a crear un servidor Yoi es necesario tener instalado NodeJS y el gestor de paquetes npm.