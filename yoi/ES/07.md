# Crons

En Yoi los Cron son rutinas en segundo plano que se ejecutan a intervalos regulares. Estas rutinas están planificadas temporalmente y permanecen a la espera hasta el momento de su ejecución. Con Yoi es muy sencillo realizar procesos cronológicos, en este capítulo veremos como crear un Cron desde cero.

## Planificar la ejecución

El primer paso para crear un Cron es indicar a través de nuestro yoi.yml el nombre del Cron que queremos que se ejecute, con que frecuencia, el archivo donde implementaremos el cron y la franja horaria del time server.

yoi.yml

```
crons:
  - name    : Example of 5 seconds job
    schedule: "*/5 * * * * *"
    file    : example_cron
    timezone: Europe/Madrid
```
En este caso vamos a decirle que se ejecute cada 5 segundos pero podemos planificarlo de la manera que mejor nos convenga modificando el campo schedule con este sistema

```
.---------------- minuto (0 - 59) 
|  .------------- hora (0 - 23)
|  |  .---------- día del mes (1 - 31)
|  |  |  .------- mes (1 - 12) O jan,feb,mar,apr ... (los meses en inglés)
|  |  |  |  .---- día de la semana (0 - 6) (Domingo=0 ó 7) O sun,mon,tue,wed,thu,fri,sat (los días en inglés) 
|  |  |  |  |
*  *  *  *  *  
```

De esta manera podemos decirle que se ejecute a una hora determinada o con una frecuencia específica.

Ejemplos:

1.	Cada 2 horas: ```0 0-23/2 * * *```
2.	A media noche: ```1 0 * * *```
3.	Lunes a las 10:30 : ```30 10 * * 1```

Con la ejecución ya planificada solo nos queda implementar y dotar de funcionalidad a nuestro cron.

## Implementando el cron

Creamos en la raiz de nuestro proyecto una carpeta crons con un fichero example_cron.coffee, y aquí sera donde implementaremos las acciones del cron. 

Lo primero es crear una clase que extienda de Yoi.cron.

```
"use strict"

Yoi = require "yoi"

class ExampleCron extends Yoi.Cron


exports = module.exports = ExampleCron
```

Ahora dentro implementaremos el método principal que será el que se ejecute periodicamente segun lo establecido en el archivo yml. El método execute será el que se ejecute cada pasada, en nuestro caso cada pasada sumará 1 a un contador.

```
  count = 0

  execute: ->
    count++
```

Igual que hay un método para cuando el cron se ejecute hay otro para para su ejecución, aquí es donde tenemos que declarar el comportamiento par cuando el proceso cron finalice. En nuestro caso mostrara el contador por consola.

```
  stop: ->
    super
    console.log "count: #{count}"
 ```
 
Y esto es todo, con esto conseguiremos que cada 5 segundos se visualice por consola el contador de los intervalos de 5 segundos que han transcurrido desde que se inició el cron por primera vez. Así quedaría el código.


```
"use strict"

Yoi = require "yoi"

class ExampleCron extends Yoi.Cron

  count = 0

  execute: ->
    count++
    
  stop: ->
    super
    console.log "count: #{count}"
    
exports = module.exports = ExampleCron
```
Para ejecutar el cron solo tenemos que iniciar nuestro Yoi y el Cron quedará a la espera en segundo plando.
