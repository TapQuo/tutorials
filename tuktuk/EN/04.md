# Buttons

The buttons are essential elements in any web environment. Tuktuk offers different combinations for the different situations where the buttons are necessary. There are also different sizes to adapt to each context. 
Los botones son un elemento indispensable en cualquier entorno web. TukTuk ofrece unas combinaciones para las distintas ocasiones en las que hacemos uso de los botones. También distintos tamaños para adaptarse a cada contexto.

## Button with text

Scale of buttons with text:

![image](../images/04-btn-text.png)

	<div>
    	<a href="#" class="button large">Large</a>
        <a href="#" class="button secondary">Normal</a>
		<a href="#" class="button success small">Small</a>
		<a href="#" class="button alert tiny">Tiny</a>
	</div>

## Text and Icon

Combination of text and icon inside a button

![image](../images/04-btn-text+icon.png)

	<div>
    	<a href="#" class="button large"><span class="icon ok"></span>Large</a>
        <a href="#" class="button secondary"><span class="icon ok"></span>Normal</a>
        <a href="#" class="button success small"><span class="icon ok"></span>Small</a>
        <a href="#" class="button alert tiny"><span class="icon ok"></span>Tiny</a>
    </div>

## Only icon

Sometimes a picture is worth a thousand words, in this case an icon.

![image](../images/04-btn-icon.png)

	<div>
    	<a href="#" class="button large icon ok"></a>
        <a href="#" class="button secondary icon ok"></a>
        <a href="#" class="button success small icon ok"></a>
        <a href="#" class="button alert tiny icon ok"></a>
    </div>

## Button + Anchor

As sometimes it is necessary to stand out some buttons, the anchor property gives more importance by adjusting to the width of the container where they are visualized. 

![image](../images/04-btn-anchor.png)

	<div>
    	<a href="#" class="button anchor secondary"><span class="icon ok"></span>Anchor</a>
    </div>