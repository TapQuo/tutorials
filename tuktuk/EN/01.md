#TukTuk tutorial

Tuktuk is a framework to design web sites and webapps. It contains a great variety of utilities to design in a rapid and intuitive way. This tutorial will teach you how to use all Tuktuk´s elements. You will understand the code and the generated view in each example.  

To start using [TukTuk](https://github.com/soyjavi/tuktuk) download the this library [here](https://github.com/soyjavi/package-tuktuk/archive/0.7.4.zip) and load it in your html. 

The styles and icons:

    <!-- TUKTUK -->
    <link rel="stylesheet" href="package/tuktuk.css">
    <link rel="stylesheet" href="package/tuktuk.theme.css">
    <!-- TUKTUK.WIDGETS -->
    <link rel="stylesheet" href="package/tuktuk.icons.css">
    
And the javascript.

	<script src="package/tuktuk.js"></script>
	
tuktuk.css contains the base of the framework, tuktuk.them.css contains a personalized layer styles and finally the icons pack.   

The script is not necessary for most of the frameworks but it is for the correct functioning of some structures like modals.

