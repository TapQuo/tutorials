# Tables y Lists

## Tables

The tables are a very versatile container, they help you to put in order the information and visualise the data together. 

![image](../images/05-table.png)

The head inside the thead label is stood out in another colour in order to estress the context of the information. 

	<table>
        <thead>
            <tr>
                <th>Item</th>
                <th>Category</th>
                <th>Version</th>
                <th><span class="right">Price</span></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>iPhone</td>
                <td>Mobile</td>
                <td>5</td>
                <td><span class="right">$ 699</span></td>
            </tr>
            <tr>
                <td>iPad Mini</td>
                <td>Tablet</td>
                <td>1</td>
                <td><span class="right">$ 399</span></td>
            </tr>
            <tr>
                <td>iPad</td>
                <td>Tablet</td>
                <td>4</td>
                <td><span class="right">$ 499</span></td>
            </tr>
            <tr>
                <td>Macbook Air 13"</td>
                <td>Laptop</td>
                <td>2</td>
                <td><span class="right">$ 1299</span></td>
            </tr>
            <tr>
                <td>Macbook Pro 15"</td>
                <td>Laptop</td>
                <td class="highlight">Retina</td>
                <td><span class="right">$ 2299</span></td>
            </tr>
        </tbody>
    </table>

As you can see in the example of retina data, it is possible to stress a table´s cell through the class highlight. 


	<td class="highlight">Retina</td>
	
## Lists

There are two types of lists: ordered and disordered. Inside the disordered ones, you have different marker styles for the list elements.

### Ordered

![image](../images/05-list-numbered.png)

    <ol class="square">
        <li>Cras mattis consectetur purus sit amet fermentum.</li>
        <li>
            Donec ullamcorper nulla non metus auctor fringilla.
            <ol>
                <li>Aenean eu leo quam.</li>
                <li>Pellentesque ornare sem lacinia quam venenatis vestibulum.</li>
                <li>Nullam quis risus eget urna mollis ornare vel eu leo.</li>
            </ol>
        </li>
        <li>Maecenas sed diam eget risus varius blandit sit amet non magna.</li>
    </ol>

### Disordered

With normal markers.

![image](../images/05-list-normal.png)

    <ul>
        <li>Cras mattis consectetur purus sit amet fermentum.</li>
        <li>
            Donec ullamcorper nulla non metus auctor fringilla.
            <ul>
                <li>Aenean eu leo quam.</li>
                <li>Pellentesque ornare sem lacinia quam venenatis vestibulum.</li>
                <li>Nullam quis risus eget urna mollis ornare vel eu leo.</li>
            </ul>
        </li>
        <li>Maecenas sed diam eget risus varius blandit sit amet non magna.</li>
    </ul>
    
With circular empty markers:

![image](../images/05-list-circle.png)
    
    <ul class="circle">
        <li>Cras mattis consectetur purus sit amet fermentum.</li>
        <li>
            Donec ullamcorper nulla non metus auctor fringilla.
            <ul>
                <li>Aenean eu leo quam.</li>
                <li>Pellentesque ornare sem lacinia quam venenatis vestibulum.</li>
                <li>Nullam quis risus eget urna mollis ornare vel eu leo.</li>
            </ul>
        </li>
        <li>Maecenas sed diam eget risus varius blandit sit amet non magna.</li>
    </ul>

With square markers:

![image](../images/05-list-square.png)
    
    <ul class="square">
        <li>Cras mattis consectetur purus sit amet fermentum.</li>
        <li>
            Donec ullamcorper nulla non metus auctor fringilla.
            <ul>
                <li>Aenean eu leo quam.</li>
                <li>Pellentesque ornare sem lacinia quam venenatis vestibulum.</li>
                <li>Nullam quis risus eget urna mollis ornare vel eu leo.</li>
            </ul>
        </li>
        <li>Maecenas sed diam eget risus varius blandit sit amet non magna.</li>
    </ul>